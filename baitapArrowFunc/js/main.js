list=["pallet","viridian","pewter","cerulean","vermillion","lavender","celadon","saffron","fuschia","cinnabar"];
var item_color=(color)=>{
   var content=``;
    if(color=="pallet"){
      content=`
     <div id="${color}" class="color-button ${color} active"></div>
    `
    }else{
      content=`
     <div id="${color}" class="color-button ${color}"></div>
    `
    }
   return content;
}
function render_list_color(list){
   var content_html=``;
 for(var i=0;i<list.length;i++){
   content_html+=item_color(list[i])
 }
 document.querySelector('#colorContainer').innerHTML=content_html;
}
render_list_color(list);
document.querySelectorAll(".color-button").forEach((color,index_color,arr_cl)=>{
     color.addEventListener("click",()=>{
          document.querySelector(`.color-button-container .active`).classList.remove("active");
          color.classList.add("active");
        document.querySelector("#house").className=`house ${list[index_color]}`;
     })
})



